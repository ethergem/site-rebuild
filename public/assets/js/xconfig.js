let web3Injected = window.web3;

if(typeof web3Injected !== 'undefined'){
  console.log("Forcing EtherGem web3 injection!");
  web3 = new Web3(new Web3.providers.HttpProvider("https://lb.rpc.egem.io" || "http://localhost:8895"));
} else {
  console.log("EtherGem web3 injected!");
  web3 = new Web3(new Web3.providers.HttpProvider("https://lb.rpc.egem.io" || "http://localhost:8895"));
}

function qBlockPage() {
    var x = document.querySelector('[name="blockInput"]').value;
    window.location.replace('/block.html?='+x);
}

function qBalancePage() {
    var x = document.querySelector('[name="addressInput"]').value;
    window.location.replace('/address.html?='+x);
}

function searchBar() {
  var checkBox = document.getElementById("nodeCheck");
  var str = document.querySelector('[name="mainInput"]').value;
  console.log(str);
  // regex
  var resultAddress = str.match(/^0x[a-fA-F0-9]{40}$/)
  var resultTx = str.match(/^0x[a-fA-F0-9]{64}$/)
  var resultBlock = str.match(/^[0-9][0-9]*$/)

  if (checkBox.checked == true){
    if (resultAddress != null) {
      window.location.replace('/nodeprofile.html?='+str);
    }
  } else {
    if (resultAddress != null) {
      window.location.replace('/address.html?='+str);
    }
    if (resultTx != null) {
      window.location.replace('/txs.html?='+str);
    }
    if (str == "latest") {
      window.location.replace('/block.html?=latest');
    }
    if (str == "bot") {
      window.location.replace('/address.html?=0xeb0d2004fc6fe660c53dcee40c1d987df691329c');
    }
    if (resultBlock != null) {
      window.location.replace('/block.html?='+str);
    }
  }
}

document.body.onload=function(){
    
    async function loadWeb3Objects() {
        let block = await web3.eth.getBlockNumber();
        var element = document.getElementById('currentBlock');
        if (typeof(element) != 'undefined' && element != null)
        {
          document.getElementById('currentBlock').innerHTML = new Intl.NumberFormat().format(block);
        }
    }
    loadWeb3Objects();
    
    async function loadAPIObjects(){
        var element = document.getElementById('motd');
        if (typeof(element) != 'undefined' && element != null)
        {
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                let array = JSON.parse(this.responseText);
                document.getElementById("totalUsers").innerHTML = new Intl.NumberFormat().format(array["settings"][0]["totalUsers"]);
                document.getElementById("totalNodes").innerHTML = new Intl.NumberFormat().format(array["settings"][0]["totalNodes"]);
                document.getElementById("usersEarning").innerHTML = new Intl.NumberFormat().format(array["settings"][0]["usersEarning"]);
                document.getElementById("motd").innerHTML = array["settings"][0]["motd"];
                document.getElementById("currentSupply").innerHTML = new Intl.NumberFormat().format(array["settings"][0]["currentSupply"]);
                document.getElementById("currentMcap").innerHTML = new Intl.NumberFormat().format(Number(array["settings"][0]["currentMcap"]).toFixed(0));
                document.getElementById("roundPay").innerHTML = new Intl.NumberFormat().format(Number(array["settings"][0]["roundPay"]/Math.pow(10,18)*4*24));
                document.getElementById("roundPayHourly").innerHTML = new Intl.NumberFormat().format(Number(array["settings"][0]["roundPay"]/Math.pow(10,18)*4));
                document.getElementById("avgusd").innerHTML = new Intl.NumberFormat().format(array["settings"][0]["avgusd"]);
            }
          };
          xhttp.open("GET", "https://api.egem.io/summary", true);
          xhttp.send();
        }
    }
    loadAPIObjects();
    
    async function getBalance() {
      let hotwallet = "0xe3696bd9aeec0229bb9c98c9cc7220ce37852887";
      let coldmultisig = "0x87045b7badac9c2da19f5b0ee2bcea943a786644";
        
      var element = document.getElementById('wallet');
        if (typeof(element) != 'undefined' && element != null)
        {
          var wallet = await web3.eth.getBalance(hotwallet,function(error, result){
             if(!error){
               document.getElementById("wallet").innerHTML = result/Math.pow(10,18);
             } else {
               console.error(error);
             }
          })
          var multisig = await web3.eth.getBalance(coldmultisig ,function(error, result){
             if(!error){
               document.getElementById("multisig").innerHTML = result/Math.pow(10,18);
             } else {
               console.error(error);
             }
          })
        }

    }
    getBalance();
    
    async function getUserNodes(){
        var element1 = document.getElementById('nodeList');
        if (typeof(element1) != 'undefined' && element1 != null)
        {
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                
                let nodeArrayRaw = JSON.parse(this.responseText);
                let nodeArray = nodeArrayRaw['profiles'];
                for (var i = 0; i < nodeArray.length; i++){
                    document.getElementById("nodeList").innerHTML += "<a href='https://explorer.egem.io/addr/"+ nodeArray[i]['address']+"'>"+nodeArray[i]['address']+"</a><br>";
                }
                
            }
          };
          xhttp.open("GET", "https://api.egem.io/nodelist", true);
          xhttp.send();
        }
        var element2 = document.getElementById('usersEarning');
        if (typeof(element2) != 'undefined' && element2 != null)
        {
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                let array = JSON.parse(this.responseText);
                document.getElementById("usersEarning").innerHTML = new Intl.NumberFormat().format(array["settings"][0]["usersEarning"]);
                document.getElementById("totalNodes").innerHTML = new Intl.NumberFormat().format(array["settings"][0]["totalNodes"]);
            }
          };
          xhttp.open("GET", "https://api.egem.io/summary", true);
          xhttp.send();
        }
    }
    getUserNodes();
    
    async function pollAddress(){
        var element = document.getElementById('balance');
        if (typeof(element) != 'undefined' && element != null)
        {
                var url = location.href;
                var params = url.split('?=');
                var x = params[1];
                if(x == null) {
                    return;
                }
                var resultAddress = x.match(/^0x[a-fA-F0-9]{40}$/)
                if (resultAddress != null) {
                    var wallet = await web3.eth.getBalance(x,function(error, result){
                     if(!error){
                       document.getElementById("balance").innerHTML = result/Math.pow(10,18);
                     } else {
                       console.error(error);
                     }
                    })
                    var sfrxwallet = await web3.eth.getBalance(x, 1530000, function(error, result){
                       if(!error){
                         document.getElementById("sfrxbalance").innerHTML = (result/Math.pow(10,18) * 2);
                       } else {
                         console.error(error);
                       }
                    })
                }
        }
    }
    pollAddress();
    
    async function pollBlock(){
        var element = document.getElementById('hash');
        if (typeof(element) != 'undefined' && element != null)
        {
            var url = window.location.href;
            var params = url.split('?=');
            var x = params[1];
            var input = x;
            if (x == null) {
              return;
            }
            var cBlock = await web3.eth.getBlock(input, function(error, result){
               if(!error && result != null){
                 web3.eth.getBlockTransactionCount(input, function(error, result){
                   if(!error){
                     document.getElementById("btransactions").innerHTML = `Transactions: `+result;
                   } else {
                     document.getElementById("btransactions").innerHTML = "No data"
                   }
                 })
                 web3.eth.getBlockUncleCount(input, function(error, result){
                   if(!error){
                     document.getElementById("buncles").innerHTML = `Uncles: `+result;
                   } else {
                     document.getElementById("buncles").innerHTML = "No data"
                   }
                 })
                 txObj = result;
                 data = JSON.stringify(txObj);
                 obj = JSON.parse(data);
                 var dateTimeString = obj.timestamp;
                 var dt = new Date(dateTimeString*1000);
                 document.getElementById("bNum").innerHTML = `Block Number: `+obj.number;
                 document.getElementById("hash").innerHTML = `Hash: `+obj.hash;
                 document.getElementById("pHash").innerHTML = `Parent Hash: `+obj.parentHash;
                 document.getElementById("miner").innerHTML = `Miner: <a href='address.html?=`+obj.miner+`'>`+obj.miner+`</a>`;
                 document.getElementById("nonce").innerHTML = `Nonce: `+obj.nonce;
                 document.getElementById("size").innerHTML = `Size: `+obj.size;
                 document.getElementById("ltransactions").innerHTML = `Transactions: `+obj.transactions;
                 document.getElementById("timestamp").innerHTML = dt;
                 document.getElementById("extraData").innerHTML = obj.extraData;
                 var i, x = "";
                 for (i in obj.transactions) {
                     x += `<a href='txs.html?=`+obj.transactions[i]+`'>`+obj.transactions[i]+`</a></br>`;
                 }
                 document.getElementById("ltransactions").innerHTML = `Transaction List: </br>`+x;
               } else {
                 document.getElementById("noblock").innerHTML = `No block found.`;
                 console.log(error);
               }
            })
        }
    }
    pollBlock();
    
    function searchEnter() {
        var element = document.getElementById('searchIt');
        if (typeof(element) != 'undefined' && element != null)
        {
            // Get the input field
            var input = document.getElementById("searchEnter");

            // Execute a function when the user releases a key on the keyboard
            input.addEventListener("keyup", function(event) {
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
              // Cancel the default action, if needed
              event.preventDefault();
              // Trigger the button element with a click
              document.getElementById("searchIt").click();
            }
            }); 
        }
    }
    searchEnter();
    
    function nodePage() {
        var element = document.getElementById('nodeTitle');
        if (typeof(element) != 'undefined' && element != null)
        {
            var url = location.href;
            var params = url.split('?=');
            var x = params[1];
            if(x == null) {
                return;
            }
            alert(x)
        }
    }
    nodePage();
}

function doChartSupply() {
  var ctx = document.getElementById('supplyTime').getContext('2d');

  var labelData = ["0","5,000","2,100,000","2,500,000","5,000,000","7,500,000","10,000,000","12,500,000","15,000,000"];
  var eraData = [0,0,0,1,2,3,4,5,6];
  var brData = [8,8,8,4,2,1,0.5,0.25,0.125];
  var frData = [1,1,1,0.75,0.5,0.25,0.1,0.05,0.025];
  var nrData = [0,0,0.5,0.5,0.5,0.25,0.25,0.25,0.125];

  var supplyChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: labelData,
      datasets: [{
          data: eraData,
          label: "Era",
          borderColor: "#3e95cd",
          fill: true
        }, {
          data: brData,
          label: "Block Reward",
          borderColor: "#3cba9f",
          fill: true
        }, {
          data: frData,
          label: "Funding Reward",
          borderColor: "#e8c3b9",
          fill: true
        }, {
          data: nrData,
          label: "Node Reward",
          borderColor: "#c45850",
          fill: true
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Block rewards over time. (Scale is in block height.)'
      }
    }
  });
}